package com.sample;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ShowQuestion extends Stage {
		
	private String answer = null;
	
	public String getAnswer(){
		return this.answer;
	}
	
	public void setAnswer(String txt){
		this.answer = txt;
	}
	
	private ShowQuestion(){}
	private ShowQuestion(String questionText, String[] answers)
	{
		Parent root = null;
		
    	//this.answer = answers[(new Random()).nextInt(answers.length)]; 
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/QuestionBox.fxml"));     
			root = fxmlLoader.load();
			QuestionBoxController controller = fxmlLoader.<QuestionBoxController>getController();
			controller.setQuestionText(questionText);
			controller.setAnswers(answers);
			controller.setCallback(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
        this.setScene(new Scene(root, 600, 300));
        this.setTitle("Question");
	}
	
	public static String askQuestion(String questionText, String[] answers)
	{
		ShowQuestion sq = new ShowQuestion(questionText, answers);
		sq.initOwner(DroolsFonts.getInstance().primaryStage);
		sq.initModality(Modality.APPLICATION_MODAL); 
		sq.showAndWait();
		return sq.getAnswer();
	}
	
}

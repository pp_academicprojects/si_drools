package com.sample;


import java.io.IOException;

import org.kie.api.KieServices;
import org.kie.api.definition.type.FactType;
import org.kie.api.logger.KieRuntimeLogger;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
 
public class DroolsFonts extends Application {
	
	public Stage primaryStage;
    private static KieServices ks;
    private static KieContainer kContainer;
    public static KieSession kSession;
    private static KieRuntimeLogger kLogger;	
	private static DroolsFonts instance;
	
	public static DroolsFonts getInstance(){
		return instance; 
	}
	
    public static void main(String[] args) {
        launch(args);
    }
    
    public void setStartScene()
    {
        Button btn = new Button();
        btn.setText("So you need a typeface");
    	btn.setMinHeight(40);
    	btn.setMinWidth(300);
    	btn.setDefaultButton(true);
        btn.setOnAction(new EventHandler<ActionEvent>() {
 
            @Override
            public void handle(ActionEvent event) {
                ks = KieServices.Factory.get();
        	    kContainer = ks.getKieClasspathContainer();
            	kSession = kContainer.newKieSession("ksession-rules");
        		kLogger = ks.getLoggers().newFileLogger(kSession, "test");
        		
                kSession.fireAllRules();
        		//kLogger.close();
            }
        });
        
        StackPane root = new StackPane();
        root.getChildren().add(btn);
        primaryStage.setTitle("Hey");
        primaryStage.setScene(new Scene(root, 450, 250));
    }
    
    public void showResult(String result)
    {   
		Parent root = null;
		kLogger.close();
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ResultBox.fxml"));     
			root = fxmlLoader.load();
			ResultBoxController controller = fxmlLoader.<ResultBoxController>getController();
			controller.setFontName(result);
			controller.setCallback(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
        primaryStage.setTitle("The best font for you");
        primaryStage.setScene(new Scene(root, 450, 250));           
    }
    
    @Override
    public void start(Stage primaryStage) {
    	this.primaryStage = primaryStage;
    	DroolsFonts.instance = this;
        primaryStage.setTitle("Hey");
        setStartScene();
        primaryStage.show();
    }
    
    public void askQuestion(String id, String text, String[] answers){
    	String answer = ShowQuestion.askQuestion(text, answers);
    	if(answer == null) {
    		this.setStartScene();
    		return;
    	}

		FactType answerType = kSession.getKieBase().getFactType( "com.sample", "Answer" );
		try {
    		Object answerObject = answerType.newInstance();
    		answerType.set(answerObject, "question", id);
    		answerType.set(answerObject, "answer", answer);
    		kSession.insert(answerObject);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
  
    }    
}

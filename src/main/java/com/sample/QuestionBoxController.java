package com.sample;

import java.io.InputStream;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class QuestionBoxController {
	
	@FXML private Label questionText;
	@FXML private GridPane answersGridPane;
	ShowQuestion callback;
	
	@FXML public void initialize() {
		
	}
	
	public void setQuestionText(String txt) {
		questionText.setText(txt);
	}
	
	public void setCallback(ShowQuestion callback) {
		this.callback = callback;
	}
	
	public void setAnswers(String[] answers) {
    	for (int i = 0; i < answers.length; i++){
            Button btn = new Button();
            btn.setText(answers[i].substring(1));
            btn.setMinWidth(100);
            InputStream stream = getClass().getResourceAsStream("/images/" + answers[i] + ".png");
            if(stream != null) {
	            btn.setGraphic(new ImageView(new Image(stream, 40, 40, true, true)));
	            btn.setContentDisplay(ContentDisplay.TOP);
            }
            else {
            	btn.setMinHeight(40);
            }
            btn.setOnAction(new EventHandler<ActionEvent>() {
     
                @Override
                public void handle(ActionEvent event) {
            		callback.setAnswer("T" + btn.getText());
            		Stage stage = (Stage) answersGridPane.getScene().getWindow();
            		stage.close();
            		
                }
            });
            ColumnConstraints col = new ColumnConstraints();
            col.setPercentWidth(100/answers.length);
            answersGridPane.add(btn, i, 0, 1, 1);
            GridPane.setHalignment(btn, HPos.CENTER);
            answersGridPane.getColumnConstraints().add(col);
    	}
	}
	
	
	
	
}

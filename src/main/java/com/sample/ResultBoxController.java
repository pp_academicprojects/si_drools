package com.sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class ResultBoxController {
	
	@FXML private Label fontName;
	DroolsFonts callback;
	
	@FXML public void initialize() {
		
	}

	@FXML private void restartButtonAction(ActionEvent event) {
		this.callback.setStartScene();
	}

	public void setCallback(DroolsFonts callback) {
		this.callback = callback;
	}
	
	public void setFontName(String str) {
		fontName.setText(str);
	}

}
